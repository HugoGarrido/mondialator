app.factory('Day', function($q, $resource, $http, $rootScope) {

	var factory =  {
		get : function(){
			var deferred = $q.defer();

			if($rootScope.dayRequested == false){

				$http.get("/api/get/today").then(function(response){
					var day = response.data.day[0];
					$rootScope.day = day;
					factory.data = day;
					deferred.resolve(factory.data);
				});

				return deferred.promise;
			}
			else{

				factory.data = $rootScope.day;
				deferred.resolve(factory.data);

				return deferred.promise;
			}		
		}
 	};
 	return factory;
});