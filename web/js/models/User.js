app.factory('User', function($q, $resource, $http, $rootScope, localStorageService) {

	var factory =  {
		data : false,
		all : false,
		load : function(id){
			var deferred = $q.defer();
			
			$http.get("/api/users/"+id).then(function(response){
				var user = response.data.data[0];

				if(user.country != null){
					user.country.isoLower = user.country.iso.toLowerCase();
				}

				user.followLength = user.follow.length;
				user.followingLength = user.following.length;
				
				user.submissionLength = user.submission.length;				
				user.achievementsLength = user.achievements.length;
				
				factory.data = user;
				deferred.resolve(factory.data);
			});

			return deferred.promise;	
		},
		loadAll : function(){
			var deferred = $q.defer();

			var r = $resource('/api/users', {});

			var user = r.query({}, function() {
	            factory.all = user;
	            deferred.resolve(factory.all);
	        });

	        return deferred.promise;
		},
		get : function(){
			var deferred = $q.defer();

			if($rootScope.user == null){

				if(localStorageService.get('user') == null || localStorageService.get('user').id == undefined){
					
					$http.get("/api/users/get").then(function(response){
						var user = response.data.user[0];
						
						if(user.achievements.length > 0){
							user.achievements = user.achievements[user.achievements.length - 1];	
						}
						else{
							user.achievements = null;
						}
						
						if(user.country.iso){
							user.country.isoLower = user.country.iso.toLowerCase();
						}
						
						//user.follow = user.follow;
						user.followLength = user.follow.length;
						//user.following = user.following;
						user.followingLength = user.following.length;

						// Save user into local storage
						localStorageService.set('user', user);

						// Resolve the promise
						factory.data = user;
						// Make user accessible in rootscope
						$rootScope.user = user;
						deferred.resolve(factory.data);
					});
				}
				else{
					$rootScope.user = localStorageService.get('user');
					factory.data = $rootScope.user;
					deferred.resolve(factory.data);
				}

				return deferred.promise;
			}
			else{

				factory.data = $rootScope.user;
				deferred.resolve(factory.data);

				return deferred.promise;
			}	
		}
 	};
 	return factory;

});