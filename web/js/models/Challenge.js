app.factory('Challenge', function($q, $resource, $http, $rootScope) {

	var factory =  {
		canPublish : function(challengeId){
			
			var deferred = $q.defer();
			
			$http.get("/api/submission/check/"+challengeId).then(function(response){
				factory.data = !response.data.existingSubmission;
				$rootScope.canPublish = factory.data;
				deferred.resolve(factory.data);
			});

			return deferred.promise;			
		}
 	};
 	return factory;
	
});