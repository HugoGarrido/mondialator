var app = angular.module('mondialatorApp', ['ngResource','ngRoute', 'satellizer', 'LocalStorageModule']);


app.config(function($authProvider, $routeProvider, $locationProvider, localStorageServiceProvider){

	/*$routeProvider.when('/', {
		templateUrl : 'views/home.html',
		controller : 'HomeCtrl'
	});*/

	$routeProvider.when('/login', {
		templateUrl : 'views/login.html',
		controller : 'LoginCtrl',
		resolve: {
		  loginRequired: skipIfLoggedIn
		}
	});	

	$routeProvider.when('/logout', {
		templateUrl : 'views/logout.html',
		controller : 'LogoutCtrl',
		resolve: {
			loginRequired: loginRequired
		}
	});	

	$routeProvider.when('/feed', {
		templateUrl : 'views/feed.html',
		controller : 'FeedCtrl',
		resolve: {
			loginRequired: loginRequired,
			load: function(User) {
				return User.get();
			},			
			day: function(Day) {
				return Day.get();
			}
		}
	});

	$routeProvider.when('/world', {
		templateUrl : 'views/feed.html',
		controller : 'WorldCtrl',
		resolve: {
			loginRequired: loginRequired,
			load: function(User) {
				return User.get();
			},			
			day: function(Day) {
				return Day.get();
			}
		}
	});

	$routeProvider.when('/leaderboard', {
		templateUrl : 'views/leaderboard.html',
		controller : 'LeaderboardCtrl',
		resolve: {
			loginRequired: loginRequired,
			get: function(User) {
				return User.get();
			},			
			day: function(Day) {
				return Day.get();
			}
		}
	});

	$routeProvider.when('/publish/:id', {
		templateUrl : 'views/upload.html',
		controller : 'UploadCtrl',
		resolve : {
			loginRequired: loginRequired,
			get: function(User) {
				return User.get();
			},			
			day: function(Day) {
				return Day.get();
			},
			canPublish: function($route, Challenge) {
				return Challenge.canPublish($route.current.params.id)
			}
		}
	});

	$routeProvider.when('/user/:id', {
		templateUrl : 'views/user.html',
		controller : 'UserCtrl',
		resolve: {
			loginRequired: loginRequired,
			get: function(User) {
				return User.get();
			},
			load : function($route, User){
				return User.load($route.current.params.id);
			},		
			day: function(Day) {
				return Day.get();
			}
		}
	});

	$routeProvider.otherwise('/login');

	/**
	 * Auth Login
	 */

	$authProvider.tokenName = 'token';
	$authProvider.tokenPrefix = 'mondialator';
	$authProvider.loginUrl = '/api/users/login';

	$authProvider.facebook({
		url: '/api/users/login/facebook',
	    clientId: FB_ID,
	});
	
	function skipIfLoggedIn($q, $auth, $location) {
	    var deferred = $q.defer();
	    if ($auth.isAuthenticated()) {
	        $location.path('/feed');
	    } else {
	        deferred.resolve();
	    }
	    return deferred.promise;
	}

	function loginRequired($q, $location, $auth) {
	    var deferred = $q.defer();
	    if ($auth.isAuthenticated()){
	        deferred.resolve();
	    }
	    else {
	        $location.path('/login');
	    }
	    return deferred.promise;
	}


	/**
	 * Local storage user
	 */
	
	localStorageServiceProvider.setPrefix('mondialatorApp');
	localStorageServiceProvider.setStorageType('localStorage');
});


app.run(function($rootScope, $auth, $location, localStorageService, $http){
	/**
	 * Settings and utils
	 */
	
	$rootScope.user = null;

	$rootScope.day = null;
	$rootScope.dayRequested = false;
	
	$rootScope.challenge = null;
	$rootScope.challengeRequested = false;


	$rootScope.openMenu = false;

	$rootScope.showMe = function() {
		$rootScope.openMenu = !$rootScope.openMenu;
	};

	$rootScope.getFromTop= function(){
		var h = document.getElementById('mondialatorNav');
		var n = document.querySelector("#mondialatorDock nav");
		var mT = parseInt(window.getComputedStyle(n).marginTop)
		var offset = parseInt(window.getComputedStyle(h).height) + mT;
		var val = window.innerHeight - offset;
		
		offset = String (offset)+'px';
		val = String (val)+'px';
		mT = String (mT)+'px';

		var obj = {
			'offset' : offset,
			'height' : val,
			'marginTop' : mT
		}

		return obj;
	}

	/**
	 * Loading state
	 */

	$rootScope.$on('$routeChangeStart', function(e, curr, prev) { 
	    if (curr.$$route && curr.$$route.resolve) {
	      	// Show a loading message until promises aren't resolved
	      	if($rootScope.user != null){
	      		$rootScope.loadingView = false;
				$rootScope.loadingContextView = true;
				return;
			}
		   	$rootScope.loadingView = true;	
	    }
	});

	$rootScope.$on('$routeChangeSuccess', function(e, curr, prev) { 
	   	// Hide loading message
	   	if($rootScope.user != null){
	   		
	   		if($rootScope.loadingView == true){
	   			$rootScope.loadingView = false;
	   		}

			$rootScope.loadingContextView = false;
			return;	
	   	}
	   	
	   	$rootScope.loadingView = false;	
	});


	/**
	 * Update User data in local
	 */

	$rootScope.updateScore = function(){
		if($rootScope.user != null) {
			$http.get("/api/users/get/score").then(function(response) {
				if($rootScope.user.score != response.data.score){
					$rootScope.user.score = response.data.score;
					localStorageService.set('user', $rootScope.user);
				}
			});
		}
	}

	$rootScope.updateFollowers = function() {
		if($rootScope.user != null) {
			$http.get("/api/users/get/followers").then(function(response) {
				if($rootScope.user.follow.length != response.data.follows.length){
					$rootScope.user.follow = response.data.follows;
					$rootScope.user.followLength = response.data.follows.length;
					localStorageService.set('user', $rootScope.user);
				}
			});
		}
	}

	$rootScope.updateAchievement = function() {
		if($rootScope.user != null) {
			$http.get("/api/users/get/achievements").then(function(response) {
				if($rootScope.user.achievements.id != response.data.achievements.id){
					$rootScope.user.achievements = response.data.achievements;
					localStorageService.set('user', $rootScope.user);
				}
			});
		}
	}
});
