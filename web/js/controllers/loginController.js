angular.module('mondialatorApp').controller('LoginCtrl', function($scope, $auth, $location){

	/*
     * Login 
	 */
	$scope.btnDisable = false;
	$scope.loading = false;

	$scope.authenticate = function(provider) {
		$scope.btnDisable = true;
		$scope.loading = true;
	    $auth.authenticate(provider, {prodiver : provider})
	    .then(function(response){
	    	if(response.status == 200){
	    		$auth.setToken(response.data.token);
	    		$scope.btnDisable = false;
	    		$scope.loading = false;
	    		$location.path('/me');
	    	}
	    });
	};
});
