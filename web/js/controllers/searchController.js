angular.module('mondialatorApp').controller('searchCtrl', function($scope, $http, $q){

	/*
     * Search 
	 */
	$scope.searchResult = {'users':[],};
	$scope.showResult = false;
	$scope.searching = false;

	$scope.previousLengthUser = 0;
	$scope.previousLengthDay = 0;
	$scope.previousLengthSponsor = 0;

	$scope.canceller = $q.defer();

	$scope.$watch('inputSearch', function (val){
		if(val != undefined){

			if(val.length > 1){

				$scope.searching = true;

				$http.get('/api/search/basic/'+val, {'timeout' : $scope.canceller.promise}).then(
					function(response){

						/* adapt for next search fields */
						if(response.data.data.users.length > 0){
							$scope.searchResult.users = response.data.data.users;
							$scope.previousLengthUser = response.data.data.users.length;
						}

						/* adapt for next search fields */
						if($scope.previousLengthUser > 0){
							$scope.showResult = true;
							$scope.searching = false;
						}
						else{
							$scope.showResult = false;
							$scope.searching = true;
						}
					}
				);
			}

			if(val.length === 0){
				$scope.showResult = false;
				$scope.searching = false;
			}
		}
	});

	$scope.clearSearch = function() {
		$scope.inputSearch = undefined;

		$scope.showResult = false;
		$scope.searching = false;

		$scope.previousLengthUser = 0;

		$scope.searchResult.users = [];
		$scope.canceller.resolve();
		$scope.canceller = $q.defer();
	};
});