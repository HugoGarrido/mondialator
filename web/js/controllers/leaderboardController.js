angular.module('mondialatorApp').controller('LeaderboardCtrl', function($scope, $rootScope, $http){

	$scope.loading = true;

	$scope.leaderboard = [];
	$scope.boardOffset = 0;
	var step = 10;


	$http.get('/api/leaderboard/get/'+$scope.boardOffset).then(function(response){
		if(response.data.status == 200){
			$scope.leaderboard = $scope.leaderboard.concat(response.data.leaderboard);
			$scope.boardOffset += step;
			$scope.loading = false;
		}
	});

	$scope.next = function(){
		$scope.loading = true;

		$http.get('/api/leaderboard/get/'+$scope.boardOffset).then(function(response){
			if(response.data.status == 200){
				$scope.leaderboard = $scope.leaderboard.concat(response.data.leaderboard);
				$scope.boardOffset += step;
				$scope.loading = false;
			}
		});
	};

	$scope.lower = function(str){
		return str.toLowerCase();
	};
});
