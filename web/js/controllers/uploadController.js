angular.module('mondialatorApp').controller('UploadCtrl', function ($scope, $location, $rootScope, Challenge){
    
    $scope.canPublish = Challenge.data;

    $scope.uploading = false;
    $scope.send = false;
    $scope.added = false;

});