angular.module('mondialatorApp').controller('UserCtrl', function($scope, $rootScope, User, $location, $http, localStorageService){

	$scope.user = User.data;

	$scope.limit = 40;
	$scope.toShow = 'subs';

	$scope.showFollowBlock = false;
	$scope.showFollow = true;
	$scope.showUnFollow = false;

	$scope.btnDisable = false;

	/*
	 * Show block with follow btn only if it is not the user page
	 */
	if($rootScope.user.id != $scope.user.id){
		$scope.showFollowBlock = true;
		if($scope.user.follow.indexOf($rootScope.user.id) != -1){
			$scope.showUnFollow = true;
			$scope.showFollow = false;
		}
	}

	
	/*
	 * The user susbrice to another user
	 */
	$scope.follow = function(id) {
		$scope.btnDisable = true;

		$http.post('/api/users/follow/add/', {'followid' : id}).then(function(response){
			if(response.data.status == 200){
				$scope.showFollow = false;
				$scope.showUnFollow = true;

				$scope.user.followingLength += 1;
				$rootScope.user.followLength += 1;
				$scope.user.follow.push($rootScope.user.id);

				localStorageService.set('user', $rootScope.user);

				$scope.btnDisable = false;
			}
		});
	};

	
	/*
	 * The user unsusbrice to another user
	 */
	$scope.unfollow = function(id) {
		$scope.btnDisable = true;

		$http.post('/api/users/follow/remove/', {'followid' : id}).then(function(response){
			if(response.data.status == 200){
				$scope.showFollow = true;
				$scope.showUnFollow = false;

				$scope.user.followingLength -= 1;
				$rootScope.user.followLength -= 1;
				var indexUserinArray = $rootScope.user.follow.indexOf($scope.user.id);
				$rootScope.user.follow.splice(indexUserinArray, 1);

				localStorageService.set('user', $rootScope.user);

				$scope.btnDisable = false;
			}
		});
	};

	/*
	 * Show specific tab
	 */
	$scope.showPanel = function(slug){
		$scope.toShow = slug;
	};
});


