angular.module('mondialatorApp').controller('FeedCtrl', function($scope, $rootScope, User, Day, Challenge, $http, $filter, $location){
	/*
	 * Get User
	 */
	
	$scope.user = $rootScope.user;

	/*
	 * Get Day and challenge
	 */
	
	$scope.day = $rootScope.day;

	/*
	 * 
	 */
	$scope.loading = true;

	$scope.feed = [];
	$scope.feedOffset = 0;
	var step = 10;


	/**
	 * API route for user personnal feed
	 */
	
	$http.get('/api/feed/user/get/'+$scope.feedOffset).then(function(response){
		$scope.feed = response.data.submissions;
		$scope.feedOffset += step;
		$scope.loading = false;
	}).then(function(response){
		$scope.loading = false;
	});
	
	$scope.reload = function(){
		$scope.loading = true;

		$http.get('/api/feed/user/get/'+$scope.feedOffset).then(function(response){
			$scope.feed = $scope.feed.concat(response.data.submissions);
			$scope.feedOffset += step;
			$scope.loading = false;
			
		});		
	};

	$scope.niceDate = function(dateString){
		if(dateString){
			var t = dateString.split(/[- :]/);

			// Apply each element to the Date function
			var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);

			return $filter('date')(d, 'dd/MM à HH:mm');
		}
		return null;	
	};

	$scope.addLike = function(subId, index){
		$scope.feed[index].addLikeDead = true;
		$scope.feed[index].addLikeLoad = true;


		$http.post('/api/like/add', {'submissionId' : subId}).then(function(response){
			if(response.data.status == 200){
				$scope.feed[index].addLikeLoad = false;
				$scope.feed[index].thumbsLength +=1;
				$scope.feed[index].removeLikeDead = false;
				$scope.feed[index].userLiked = true;
			}
		});
	};

	$scope.removeLike = function(subId, index){
		$scope.feed[index].removeLikeDead = true;
		$scope.feed[index].removeLikeLoad = true;


		$http.post('/api/like/remove', {'submissionId' : subId}).then(function(response){
			if(response.data.status == 200){
				$scope.feed[index].removeLikeLoad = false;
				$scope.feed[index].thumbsLength -=1;
				$scope.feed[index].addLikeDead = false;
				$scope.feed[index].userLiked = false;
			}
		});
	};

	$scope.addComment = function(subId, index){
		var text = $scope.feed[index].text;
		$scope.feed[index].postingComment = true;
		$http.post('/api/comment/add', {'submissionId' : subId, 'textComment' : text}).then(function(response){
			if(response.data.status == 200){
				$scope.feed[index].text = '';
				$scope.feed[index].commentsLength +=1;
				$scope.feed[index].postingComment = false;

				$http.get('/api/comment/submission/'+subId).then(function(response){
					$scope.feed[index].comments = response.data.comments;
				});
			}
		});
	};

	$scope.switchForm = function(index){
		if($scope.feed[index].showForm == undefined){
			$scope.feed[index].showForm = true;
		}else{
			$scope.feed[index].showForm = !$scope.feed[index].showForm;
		}
	};

	$scope.switchComments = function(index){
		if($scope.feed[index].showComments == undefined){
			$scope.feed[index].showComments = true;
		}else{
			$scope.feed[index].showComments = !$scope.feed[index].showComments;
		}
	};

	$scope.isUser = function(index){
		if(index == $rootScope.user.id){
			return true;
		}
		else{
			return false;
		}
	};

	$scope.deleteComment = function(commentId, subId, index){
		$http.post('/api/comment/delete', {'commentId' : commentId, 'userId' : $rootScope.user.id}).then(function(response){
			if(response.status == 200){
				$http.get('/comment/submission/'+subId).then(function(response){
					$scope.feed[index].comments = response.data.comments;
				});
			}
		})
	};

	$scope.checkImage = function(type){
		if(type=='image') return true;
		else return false;
	};		

	$scope.checkVideo = function(type){
		if(type=='video') return true;
		else return false;
	};


	/**
	 * Update User
	 */
	
	$rootScope.updateScore();
	$rootScope.updateFollowers();
	$rootScope.updateAchievement();

});


