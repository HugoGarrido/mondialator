angular.module('mondialatorApp').controller('LogoutCtrl', function($scope, $rootScope, $auth, $location, localStorageService){

	/*
     * Logout 
	 */
	if(!$auth.isAuthenticated()){
		$location.path("/login");
	}
	else{
		$auth.logout().then(function(){
			$auth.removeToken();
			$rootScope.user = null;
			localStorageService.remove('user');
			$location.path("/login");
		});
	}
});
