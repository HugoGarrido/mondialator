angular.module('mondialatorApp').directive('dropzone', ['$window', '$auth', '$location', function ($window, $auth, $location) {
    return {
        restrict: 'AC',
        link: function (scope, element, attrs) {

            var list = scope.day.challenge[0].type.extensions.replace(/;/gi, ", .");
            list = '.' + list;

      		var config = {
                url: '/api/submission/',
                maxFilesize: 10,
                paramName: "file",
                maxThumbnailFilesize: 10,
                parallelUploads: 1,
                autoProcessQueue: false,
                dictDefaultMessage: 'Click or Drag to Publish :)',
                acceptedFiles: list
            };

            var dropzone = new Dropzone(element[0], config);

            dropzone.on("sending", function(file, xhr, formData) {
                scope.uploading = true;
                formData.append("userid", scope.user.id);
                formData.append("authorization", $auth.getToken());
                formData.append("challengeid", scope.day.challenge[0].id);
            });
            
            scope.$watch('added', function (val){

                if(val == true){
                    var button = document.getElementById('submitUpload');
                    button.addEventListener('click', function() {
                        dropzone.processQueue();
                    });
                }
            });
            

            dropzone.on("success", function(){
                scope.$apply(function(){
                    scope.send = true;

                    window.setTimeout(function(){
                        scope.$apply(function(){
                            $location.path("/feed");
                        });
                    }, 900);
                    
                });
            });

            dropzone.on("addedfile", function(){
                scope.$apply(function(){
                    scope.added = true;
                });    
            });
        }
    };
}]);
