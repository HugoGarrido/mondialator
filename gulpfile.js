var host = require('./gulp/config').host;

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var minifyCss = require('gulp-minify-css');
var rename = require("gulp-rename");
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('sass', function(){
	return gulp.src('./web/styles/sass/style.scss')
		.pipe(sass.sync().on('error', sass.logError))
		.pipe(gulp.dest('./web/styles/css'))
		.pipe(autoprefixer({browsers: ['last 2 versions'],cascade: false}));
});

gulp.task('concatCss', function(){
	return gulp.src(['./web/styles/css/reset.min.css', './web/styles/css/style.css'])
	.pipe(concat('main.css'))
	.pipe(gulp.dest('./web/styles/css'))
	.pipe(browserSync.stream());
	
});

gulp.task("minCss", function(){
	return gulp.src('./web/styles/css/main.css')
	.pipe(minifyCss())
	.pipe(rename(function(path){
		path.basename += ".min";
	}))
	.pipe(gulp.dest('./web/styles/css/min'));
});

gulp.task('concatLibs', function(){
	return gulp.src([
			'./web/js/libs/dropzone.min.js',
			'./web/js/setup.js',
			'./web/js/angular.min.js',
			'./web/js/angular-resource.min.js',
			'./web/js/angular-route.min.js',
			'./web/js/libs/satellizer.min.js',
			'./web/js/libs/angular-local-storage.min.js'
		])
	.pipe(concat('libs.min.js'))
	.pipe(gulp.dest('./web/js'))
	.pipe(browserSync.stream());
});

gulp.task('concatScripts', function(){
	return gulp.src([
		'./web/js/app.js', 
		'./web/js/models/User.js',
		'./web/js/models/Day.js', 
		'./web/js/models/Challenge.js', 
		'./web/js/controllers/feedController.js', 
		'./web/js/controllers/worldController.js',
		'./web/js/controllers/leaderboardController.js',
		'./web/js/controllers/homeController.js',
		'./web/js/controllers/usersController.js',
		'./web/js/controllers/userController.js',
		'./web/js/controllers/uploadController.js',
		'./web/js/controllers/loginController.js',
		'./web/js/controllers/logoutController.js',
		'./web/js/controllers/searchController.js',
		'./web/js/directives/upload.js'
		])
	.pipe(concat('scripts.min.js'))
	.pipe(uglify({mangle: false}))
	.pipe(gulp.dest('./web/js'))
	.pipe(browserSync.stream());
});


gulp.task('serve', ['sass', 'concatCss', 'minCss'], function(){

	browserSync.init({
		proxy: host
	});

	gulp.watch('./web/styles/sass/*.scss', ['sass']);
	gulp.watch('./web/styles/sass/*/*.scss', ['sass']);
	gulp.watch('./web/styles/css/style.css', ['concatCss']);
	gulp.watch('./web/styles/css/main.css', ['minCss']);
});

gulp.task('watch', ['sass', 'concatCss', 'minCss', 'concatScripts'], function(){
	gulp.watch('./web/styles/sass/*.scss', ['sass']);
	gulp.watch('./web/styles/sass/*/*.scss', ['sass']);
	gulp.watch('./web/styles/css/style.css', ['concatCss']);
	gulp.watch('./web/styles/css/main.css', ['minCss']);
	gulp.watch('./web/js/*/*.js', ['concatScripts'])
	gulp.watch('./web/js/*.js', ['concatScripts'])
});