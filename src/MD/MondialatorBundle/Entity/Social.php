<?php

namespace MD\MondialatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Social
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MD\MondialatorBundle\Entity\SocialRepository")
 */
class Social
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MondialatorBundle\Entity\SocialType", cascade={"persist"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MondialatorBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
    */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=255)
     */
    private $socialId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Social
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return SocialType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Social
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function setToken($token){
        $this->token = $token;
    }

    public function getToken(){
        return $this->token;
    }

    public function setSocialId($socialId){
        $this->socialId = $socialId;
    }

    public function getSocialId(){
        return $this->socialId;
    }
}

