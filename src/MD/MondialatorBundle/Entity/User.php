<?php

namespace MD\MondialatorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MD\MondialatorBundle\Entity\UserRepository")
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;
    
    /**
     * @ORM\OneToOne(targetEntity="MD\MondialatorBundle\Entity\Media", cascade={"persist"})
     */
    private $picture;

    /**
     * @ORM\ManyToMany(targetEntity="MD\MondialatorBundle\Entity\Achievement", cascade={"persist"})
     */
    private $achievements;

    /**
     * @ORM\ManyToMany(targetEntity="MD\MondialatorBundle\Entity\Interest", cascade={"persist"})
     */
    private $interests;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MondialatorBundle\Entity\Country")
     */
    private $country;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="integer", length=255)
     */
    private $score;

    /**
     * @ORM\ManyToMany(targetEntity="MD\MondialatorBundle\Entity\User", mappedBy="following")
     */
    private $followers;
    // followers = ceux qui me follow

    /**
     * @var integer
     *
     * @ORM\Column(name="nbLike", type="integer", length=255)
     */
    private $nbLike;
    //Nombre de like que j'ai recu

    /**
     * @var integer
     *
     * @ORM\Column(name="nbSubmission", type="integer", length=255)
     */
    private $nbSubmission;
    //Nombre de submissions que j'ai posté    

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="followers")
     * @ORM\JoinTable(name="following",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="following", referencedColumnName="id")}
     *      )
     */
    private $following;
    // following = ceux que je follow
    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="date")
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="date")
     */
    private $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function getPassword(){
        return $this->password;
    }

    /**
     * Set picture
     *
     *
     * @return User
     */
    public function setPicture(Media $picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return Media
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set achievement
     *
     * @param Achievement $achievement
     *
     * @return User
     */
    public function setAchievement(Achievement $achievement = null)
    {
        $this->achievements[] = $achievement;

        return $this;
    }

    public function removeAchievement(Achievement $achievement)
    {
        $this->achievements->removeElement($achievement);
    }

    /**
     * Get achievement
     *
     * @return Achievement
     */
    public function getAchievement()
    {
        return $this->achievements;
    }

    /**
     * Set following
     *
     * @param Following $following
     *
     * @return User
     */
    public function setFollowing(User $follower)
    {
        $this->following[] = $follower;

        return $this;
    }

    /**
     * Get following
     *
     * @return Following
     */
    public function getFollowing()
    {
        return $this->following;
    }

    public function removeFollowing(User $follower)
    {
        $this->following->removeElement($follower);
    }

    /**
     * Set follower
     *
     * @param Following $following
     *
     * @return User
     */
    public function setFollowers(User $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Get followers
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    public function removeFollowers(User $follower)
    {
        $this->followers->removeElement($follower);
    }



    /**
     * Set interest
     *
     * @param Interest $interest
     *
     * @return User
     */
    public function setInterest(Interest $interest)
    {
        $this->interests[] = $interest;

        return $this;
    }

    public function removeInterest(Interest $interest)
    {
        $this->interests->removeElement($interest);
    }

    /**
     * Get interest
     *
     * @return Interest
     */
    public function getInterests()
    {
        return $this->interests;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return User
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return User
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return string
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set nbLike
     *
     * @param integer $nbLike
     *
     * @return User
     */
    public function setNbLike($nbLike)
    {
        $this->nbLike = $nbLike;

        return $this;
    }

    /**
     * Get nbLike
     *
     * @return string
     */
    public function getNbLike()
    {
        return $this->nbLike;
    }

    /**
     * Set nbSubmission
     *
     * @param integer $nbSubmission
     *
     * @return User
     */
    public function setNbSubmission($nbSubmission)
    {
        $this->nbSubmission = $nbSubmission;

        return $this;
    }

    /**
     * Get nbSubmission
     *
     * @return string
     */
    public function getNbSubmission()
    {
        return $this->nbSubmission;
    }    

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return User
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
    /*
    public function setSocial(Social $social)
    {
        $this->social = $social;
    }

    public function getSocial()
    {
        return $this->social;
    }
    */
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        $this->score = 0;
        $this->interests = new ArrayCollection();
        $this->following = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->achievements = new ArrayCollection();
    }

    /*
     * Return an array with the ids of the people that the user follows
     * An array of id can be use by the front
     */
    
    public function getFollowersId()
    {
        $idArray = [];

        foreach ($this->getFollowers()->toArray() as $user) {
            array_push($idArray, $user->getId());
        }

        return $idArray;
    }

    /*
     * Return an array with the ids of the people that the user is followed by
     * An array of id can be use by the front
     */

    public function getFollowingId()
    {
        $idArray = [];

        foreach ($this->getFollowing()->toArray() as $user) {
            array_push($idArray, $user->getId());
        }

        return $idArray;
    }

    /*
     * Serialize basic user datas
     */
    public function jsonSerialize(){
        return array(
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'picture' => $this->getPicture()->jsonSerialize(),
            'country' => $this->country->jsonSerialize(),
        );
    }   
}

