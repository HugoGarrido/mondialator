<?php

namespace MD\MondialatorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Challenge
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MD\MondialatorBundle\Entity\ChallengeRepository")
 */
class Challenge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="MD\MondialatorBundle\Entity\Interest", cascade={"persist"})
     */
    private $interest;

    /**
     * @ORM\OneToOne(targetEntity="MD\MondialatorBundle\Entity\Day", cascade={"persist"})
     */
    private $day;

    /**
     * @ORM\OneToOne(targetEntity="MD\MondialatorBundle\Entity\Media", cascade={"persist"})
     */
    private $picture;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MondialatorBundle\Entity\MediaType", cascade={"persist"})
     *
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="MD\MondialatorBundle\Entity\Sponsor", cascade={"persist"})
     */
    private $sponsor;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Challenge
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Challenge
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set interest
     *
     * @param Interest $interest
     *
     * @return User
     */
    public function setInterest(Interest $interest)
    {
        $this->interests[] = $interest;

        return $this;
    }

    public function removeInterest(Interest $interest)
    {
        $this->interests->removeElement($interest);
    }

    /**
     * Get interest
     *
     * @return Interest
     */
    public function getInterests()
    {
        return $this->interests;
    }

    /**
     * Set type
     *
     * @param MediaType $type
     *
     * @return Media
     */
    public function setType(MediaType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return MediaType
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set day
     *
     * @param string $day
     *
     * @return Challenge
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return string
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set picture
     *
     * @param Media $picture
     *
     * @return Challenge
     */
    public function setPicture(Media $picture)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return Media
     */
    public function getPicture()
    {
        return $this->picture;
    }

    public function __construct()
    {
        $this->interests = new ArrayCollection();
    } 
}

