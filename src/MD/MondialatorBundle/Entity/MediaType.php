<?php

namespace MD\MondialatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaType
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MD\MondialatorBundle\Entity\MediaTypeRepository")
 */
class MediaType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var text
     *
     * @ORM\Column(name="extensions", type="text", length=255, nullable=true)
     */
    private $extensions;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MediaType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get extensions
     *
     * @return  string
     */
    public function getExtensions(){
        return $this->extensions;
    }

    /**
     * Utility function
     * Check if a string match the mediaType array
     */

    public function checkType($ext)
    {
        $ext = strtolower($ext);
        return in_array($ext, explode(";", $this->extensions));
    }
}

