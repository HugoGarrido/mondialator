<?php

namespace MD\MondialatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Submission
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MD\MondialatorBundle\Entity\SubmissionRepository")
 */
class Submission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MondialatorBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MondialatorBundle\Entity\Challenge")
     * @ORM\JoinColumn(nullable=false)
     */
    private $challenge;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MondialatorBundle\Entity\Media", cascade={"persist"})
     */
    private $media;

    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;


    /**
     * @var boolean
     *
     * @ORM\Column(name="posted", type="boolean")
     */
    private $posted;


    /**
     * @ORM\OneToMany(targetEntity="MD\MondialatorBundle\Entity\Thumb", mappedBy="thumb")
     */
    private $thumbs;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Submission
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set challenge
     *
     * @param Challenge $challenge
     *
     * @return Submission
     */
    public function setChallenge(Challenge $challenge)
    {
        $this->challenge = $challenge;

        return $this;
    }

    /**
     * Get challenge
     *
     * @return Challenge
     */
    public function getChallenge()
    {
        return $this->challenge;
    }

    /**
     * Set media
     *
     * @param Media $media
     *
     * @return Submission
     */
    public function setMedia(Media $media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set posted
     *
     * @param boolean $posted
     *
     * @return Submission
     */
    public function setPosted($posted)
    {
        $this->posted = $posted;

        return $this;
    }

    /**
     * Get posted
     *
     * @return boolean
     */
    public function getPosted()
    {
        return $this->posted;
    }

    /**
     *
     */ 
    public function __construct(){
        $this->created = new \DateTime('now');
        $this->thumbs = new ArrayCollection();
    }

    /**
     * 
     */

    public function addThumb(Thumb $thumb)
    {
        $this->thumbs[] = $thumb;
        $thumb->setSubmission($this);
        return $this;
    }

    public function removeThumb(Thumb $thumb)
    {
        $this->thumbs->removeElement($thumb);
    }

    public function getThumbs()
    {
        return $this->thumbs;
    }


    /**
     * 
     */
}

