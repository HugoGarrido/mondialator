<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MD\MondialatorBundle\Form\MediaForm;
use Symfony\Component\HttpFoundation\Request;

use MD\MondialatorBundle\Entity\Achievement;

class AchievementController extends Controller
{

    public function getAchievementsAction()
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Achievement')
        ;

        $listAchievement = $repository->findAll();

        return $this->render('MDMondialatorBundle:Achievement:getAchievement.html.twig', array(
                'listAchievement'=>$listAchievement
            )
        );
    }

    public function addAchievementAction(Request $request){
        $newAchievement = new Achievement();

        $formBuilder = $this->get('form.factory')->createBuilder('form', $newAchievement);

        // Sponsor, interets et média à rajouter :
        $formBuilder
            ->add('name', 'text')
            ->add('description', 'textarea')
            ->add('type', 'entity', array(
                    'class'    => 'MDMondialatorBundle:AchievementType',
                    'property' => 'name',
                    'multiple' => false
                )
            )
            ->add('rank', 'text')
            ->add('picture', new MediaForm())
            ->add('save', 'submit')
        ;
        $form = $formBuilder->getForm();


        $form->handleRequest($request);


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newAchievement);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirect($this->generateUrl('mondialator_achievements'));
        }

        return $this->render('MDMondialatorBundle:Achievement:addAchievement.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    public function editAchievementAction(Request $request, $id)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Achievement')
        ;

        $newAchievement = $repository->findOneById($id);

        // On crée le FormBuilder grâce au service form factory
        $formBuilder = $this->get('form.factory')->createBuilder('form', $newAchievement);

        // Sponsor, interets et média à rajouter :
        $formBuilder
            ->add('name', 'text')
            ->add('description', 'textarea')
            ->add('type', 'entity', array(
                    'class'    => 'MDMondialatorBundle:AchievementType',
                    'property' => 'name',
                    'multiple' => false
                )
            )
            ->add('rank', 'text')
            ->add('picture', new MediaForm())
            ->add('save', 'submit')
        ;
        $form = $formBuilder->getForm();


        $form->handleRequest($request);


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newAchievement);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Achievement bien enregistré.');


            return $this->redirect($this->generateUrl('mondialator_achievements'));
        }

        return $this->render('MDMondialatorBundle:Achievement:editAchievement.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    public function removeAchievementAction($id){
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Achievement')
        ;

        $achievement = $repository->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($achievement);
        $em->flush();

        return $this->render('MDMondialatorBundle:Achievement:removeAchievement.html.twig');

    }


}
