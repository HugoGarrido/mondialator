<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use MD\MondialatorBundle\Entity\Challenge;

class ChallengeController extends Controller
{
    public function addChallengeAction(Request $request, $dayId = null)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Day')
        ;

        $day = $repository->findOneById($dayId);


        $repositoryChallenge = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Challenge')
        ;

        $challengeVerif = $repositoryChallenge->findOneByDay($day);

        if(isset($challengeVerif)){
            $challenge=$challengeVerif;
        }
        else{
            $challenge = new Challenge();
            $challenge->setDay($day);
        }

        $formBuilder = $this->get('form.factory')->createBuilder('form', $challenge);

        $formBuilder
            ->add('name', 'text')
            ->add('description', 'textarea')
            ->add('type', 'entity', array(
                    'class'    => 'MDMondialatorBundle:MediaType',
                    'property' => 'name',
                    'multiple' => false
                )
            )
            ->add('save', 'submit')
        ;


        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($challenge);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirect($this->generateUrl('mondialator_days_get', array(
                        'month' => $day->getDate()->format('m')
                    )
                )
            );
        }


        return $this->render('MDMondialatorBundle:Challenge:addChallenge.html.twig', array(
                'form' => $form->createView(), 'day' => $day
            )
        );
    }

    public function editChallengeAction(Request $request, $id){

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Challenge')
        ;

        $challenge = $repository->find($id);

        $formBuilder = $this->get('form.factory')->createBuilder('form', $challenge);
        $formBuilder
            ->add('name', 'text')
            ->add('description', 'textarea')
            ->add('type', 'entity', array(
                    'class'    => 'MDMondialatorBundle:MediaType',
                    'property' => 'name',
                    'multiple' => false
                )
            )
            ->add('save', 'submit')
        ;
        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($challenge);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Challenge modifié');

            return $this->redirect($this->generateUrl('mondialator_days_get', array()));
        }

        return $this->render('MDMondialatorBundle:Challenge:editChallenge.html.twig', array(
            'form' => $form->createView(),
            'challenge' => $challenge
        ));
    }

    public function deleteChallengeAction($id){
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Challenge')
        ;

        $challenge = $repository->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($challenge);
        $em->flush();

        return $this->render('MDMondialatorBundle:Challenge:removeChallenge.html.twig', array());
    }
}
