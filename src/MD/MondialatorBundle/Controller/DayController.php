<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use MD\MondialatorBundle\Entity\Day;
use MD\MondialatorBundle\Entity\Challenge;


class DayController extends Controller
{
    /*
     * ADMIN
     */

	public function getDaysAction($month)
	{
		$repository = $this
			->getDoctrine()
			->getManager()
			->getRepository('MDMondialatorBundle:Day');

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'SELECT d
            FROM MDMondialatorBundle:Day d
            GROUP BY d.date
            ORDER BY d.date ASC'
		);

		$listDays = $query->getResult();

		return $this->render('MDMondialatorBundle:Day:getDays.html.twig', array(
				'days'=>$listDays,
				'month'=>$month
			)
		);
	}

	public function showDayAction($id)
	{

		$repository = $this
			->getDoctrine()
			->getManager()
			->getRepository('MDMondialatorBundle:Day')
		;

		$day = $repository->find($id);


		$repositoryChallenge = $this
			->getDoctrine()
			->getManager()
			->getRepository('MDMondialatorBundle:Challenge')
		;

		$listChallenge = $repositoryChallenge->findByDay($day);

		return $this->render('MDMondialatorBundle:Day:showDay.html.twig', array(
				'day'=>$day ,'listChallenge'=>$listChallenge
			)
		);
	}

	public function editDayAction(Request $request, $id)
	{
		$repository = $this
			->getDoctrine()
			->getManager()
			->getRepository('MDMondialatorBundle:Day')
		;

		$day = $repository->find($id);


		$formBuilder = $this->get('form.factory')->createBuilder('form', $day);

		$formBuilder
			->add('name', 'text')
			->add('description', 'textarea')
			->add('date', 'date')
			->add('save', 'submit')
		;

		$form = $formBuilder->getForm();

		$form->handleRequest($request);


		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($day);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

			return $this->redirect($this->generateUrl('mondialator_days_get', array(
					'month'=>$day->getDate()->format('m')
				)
			)
			);
		}

		return $this->render('MDMondialatorBundle:Day:editDay.html.twig', array(
				'form' => $form->createView()
			)
		);
	}

	public function removeDayAction($id){
		$repository = $this
			->getDoctrine()
			->getManager()
			->getRepository('MDMondialatorBundle:Day')
		;

		$achievement = $repository->findOneById($id);
		$em = $this->getDoctrine()->getManager();
		$em->remove($achievement);
		$em->flush();

		return $this->render('MDMondialatorBundle:Day:removeDay.html.twig');

	}

	public function addDayAction(Request $request)
	{

		$newDay = new Day();

		$formBuilder = $this->get('form.factory')->createBuilder('form', $newDay);

		$formBuilder
			->add('name', 'text')
			->add('description', 'textarea')
			->add('date', 'date')
			->add('save', 'submit')
		;

		$form = $formBuilder->getForm();

		$form->handleRequest($request);


		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($newDay);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');


			return $this->redirect($this->generateUrl('create_day', array(
					'month'=>$newDay->getDate()->format('m')
				)
			)
			);
		}


		return $this->render('MDMondialatorBundle:Day:addDay.html.twig', array(
				'form' => $form->createView()
			)
		);
	}

	/*
     * API
     */

	public function getDayAction(Request $request, $id)
    {
    	if($request->getMethod() == 'GET'){
	 		$repository = $this
	 			->getDoctrine()
	 			->getManager()
	 			->getRepository('MDMondialatorBundle:Day')
	 		;

	 		$day = $repository->findById($id)->getArrayResult();
            $day = $day[0];

	 		$repository = $this
	 			->getDoctrine()
	 			->getManager()
	 			->getRepository('MDMondialatorBundle:Challenge')
	 		;

	 		$challenge = $repository->findChallengeByDate($day["date"]->format("Y-m-d"))->getArrayResult();
	 		$day['challenge'] = $challenge;

			return new JsonResponse(
				array(
					'day' => $day,
					'status' => Response::HTTP_OK, 
				)
			);
    	}
 		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));
    }

    public function getDayChallengeAction(Request $request, $id)
    {
    	if($request->getMethod() == 'GET'){
	 		$repository = $this
	 			->getDoctrine()
	 			->getManager()
	 			->getRepository('MDMondialatorBundle:Challenge')
	 		;

	 		$challenge = $repository->findById($id)->getArrayResult();


			return new JsonResponse(
				array(
					'challenge'=>$challenge,
					'status' => Response::HTTP_OK, 
				)
			);
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));	
    }    

	public function getTodayAction(Request $request){
		if($request->getMethod() == 'GET'){
			$repository = $this
				->getDoctrine()
				->getManager()
				->getRepository('MDMondialatorBundle:Day')
			;

			$day = $repository->findDayByDate(date('Y-m-d'))->getArrayResult();

			$repository = $this
				->getDoctrine()
				->getManager()
				->getRepository('MDMondialatorBundle:Challenge')
			;

			$challenge = $repository->findChallengeByDate(date('Y-m-d'))->getArrayResult();

			$day[0]['challenge'] = $challenge;
			return new JsonResponse(
				array(
					'day' => $day,
					'status' => Response::HTTP_OK,
				)
			);
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));
	}


	public function getTodayChallenge(Request $request){
		if($request->getMethod() == 'GET'){
			$repository = $this
				->getDoctrine()
				->getManager()
				->getRepository('MDMondialatorBundle:Challenge')
			;

			$challenge = $repository->findChallengeByDate(date('Y-m-d'))->getArrayResult();


			return new JsonResponse(
				array(
					'challenge'=>$challenge,
					'status' => Response::HTTP_OK,
				)
			);
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));
	}

}
