<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use MD\MondialatorBundle\Form\MediaForm;

use MD\MondialatorBundle\Entity\Interest;

class InterestController extends Controller
{

    public function getInterestsAction()
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Interest')
        ;

        $listInterest = $repository->findAll();

        return $this->render('MDMondialatorBundle:Interest:getInterest.html.twig', array(
                'listInterest'=>$listInterest
            )
        );
    }

    public function addInterestAction(Request $request)
    {

        $newInterest = new Interest();
        $formBuilder = $this->get('form.factory')->createBuilder('form', $newInterest);

        $formBuilder
            ->add('name', 'text')
            ->add('description', 'textarea')
            ->add('picture', new MediaForm())
            ->add('save','submit')
        ;


        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newInterest);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirect($this->generateUrl('mondialator_interests'));
        }

        return $this->render('MDMondialatorBundle:Interest:addInterest.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    public function editInterestAction(Request $request, $id){


        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Interest')
        ;

        $newInterest = $repository->findOneById($id);


        $formBuilder = $this->get('form.factory')->createBuilder('form', $newInterest);

        $formBuilder
            ->add('name', 'text')
            ->add('description', 'textarea')
            ->add('picture', new MediaForm())
            ->add('save','submit')
        ;


        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newInterest);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirect($this->generateUrl('mondialator_interests'));
        }

        return $this->render('MDMondialatorBundle:Interest:editInterest.html.twig', array('form' => $form->createView()));
    }

    public function removeInterestAction($id){
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Interest')
        ;

        $interest = $repository->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($interest);
        $em->flush();

        return $this->render('MDMondialatorBundle:Interest:removeInterest.html.twig');

    }

}
