<?php

namespace MD\MondialatorBundle\Controller;

use MD\MondialatorBundle\Entity\User;
use MD\MondialatorBundle\Entity\Media;
use MD\MondialatorBundle\Entity\Social;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp;
use \Firebase\JWT\JWT;

class UserController extends Controller
{
 	public function defaultAction(){
 		return $this->render('MDMondialatorBundle:User:index.html.twig');
 	}

 	/**
 	 * Return an user to put in profile view
 	 */

 	public function viewAction($id) {
 		$em = $this
 			->getDoctrine()
 			->getManager()
 		;	

 		$repository = $em->getRepository('MDMondialatorBundle:User');

 		$user = $repository->findUserById($id)->getArrayResult();
 		
 		$u = $repository->find($user[0]['id']);
 		
 		$follow = $u->getFollowersId();
 		$following = $u->getFollowingId();


 		$followAllData = $u->getFollowers();
 		$followingAllData = $u->getFollowing();
 		
 		$followAll = [];
 		$followingAll = [];
 		
 		foreach ($followAllData as $follower) {
 			array_push($followAll, $follower->jsonSerialize());
 		}

 		foreach ($followingAllData as $follower) {
 			array_push($followingAll, $follower->jsonSerialize());
 		}

 		$user[0]['followAll'] = $followAll;
 		$user[0]['followingAll'] = $followingAll;

 		$repository = $em->getRepository('MDMondialatorBundle:Submission');
 		$subs = $repository->getSubmissionByUser($u)->getArrayResult();
 		$user[0]['submission'] = $subs;
 		
 		$user[0]['follow'] = $follow;
 		$user[0]['following'] = $following;

 		return new JsonResponse(
 			array(
 				"data" => $user,
 				"status" => Response::HTTP_OK,
 			)
 		);
 	}

 	public function addAction(Request $request) {
 		
 		if($request->getMethod()=='POST'){

	 		$user = new User();
	 		$user->setFirstname($request->get('firstname'));
	 		$user->setLastname($request->get('lastname'));
	 		$user->setEmail($request->get('email'));
	 		$user->setPassword($request->get('password'));

	 		$em= $this->getDoctrine()->getManager();

	 		$em->persist($user);

	 		$em->flush();

	 		return new Response("User ajouté avec l'id ".$user->getId());

	 	}
 	}

	/**
     * Login using oauth2
     * return: jwt token 
     */
   
	public function loginAction(Request $request, $provider) {
		if($request->getMethod() == 'POST'){

			$em= $this->getDoctrine()->getManager();
			
			if($provider == "mondialator"){

				$params = [
					'email' => $request->request->get('email'),
		            'password' => $request->request->get('password')
				];

				$userDatas = $this->loginMondialator($params);
			}
			else{
				$providerId = $em->getRepository('MDMondialatorBundle:SocialType')
					->findBy(array('name' => $provider))
				;
				
				$params = [
					'code' => $request->request->get('code'),
		            'client_id' => $request->request->get('clientId'),
		            'redirect_uri' => $request->request->get('redirectUri'),
				];
				
				$userDatas = $this->handleProvider($params, $provider);
			}
			

			/* 
			 * Create User
			*/ 
			if($userDatas['isKnowUser'] == false){
				$em= $this->getDoctrine()->getManager();

				$user = new User();
				$user->setFirstname($userDatas['first_name']);
		 		$user->setLastname($userDatas['last_name']);
		 		$user->setEmail($userDatas['email']);
		 		$user->setCountry($userDatas['locale']);
		 		$user->setScore(100);
		 		$user->setNbLike(0);
		 		$user->setNbSubmission(0);
		 		$user->setAchievement($em->getRepository('MDMondialatorBundle:Achievement')->find(1));
		 		
		 		$pict = new Media($em);
		 		$pict->setUrl($userDatas['picture']);
		 		$pict->setAlt($userDatas['name']);
		 		$user->setPicture($pict);
		 		
		 		$em->persist($user);

		 		$socialType = $em->getRepository('MDMondialatorBundle:SocialType')
		 			->findBy(['name' => $provider]);

		 		$social = new Social();
		 		$social->setSocialId($userDatas['id']);
		 		$social->setToken($userDatas['accessToken']);
		 		$social->setUser($user);
		 		$social->setType($socialType[0]);
		 		
		 		$em->persist($social);

		 		$em->flush();
				
				return new JsonResponse(
					array(
						'token' => $this->createToken($user),
						'status' => Response::HTTP_OK 
					)
				);
			}
			else{
				// User is in DB
				return new JsonResponse(
					array(
						'token' => $this->createToken($userDatas['knowUser']),
						'status' => Response::HTTP_OK, 
					)
				);
			}
		}

		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));
	}

	/**
	 * Handle the provider with params
	 */

	public function handleProvider($params, $provider){
		if($provider == "facebook"){
			return $this->loginFacebook($params);
		}
	}

	/**
	 * Handle facebook login
	 * return: users infos
	 */

	public function loginFacebook($params){
		$params['client_secret'] = $this->getParameter('facebook_secret');
		
		$urlMe = 'https://graph.facebook.com/v2.5/me';
		$urlAccess = 'https://graph.facebook.com/v2.5/oauth/access_token';

		$default = ["verify" => $this->get('kernel')->getRootDir().'/../cert.pem'];

		$client = new GuzzleHttp\Client($default);

		$accessTokenResponse = $client->request('GET', $urlAccess, ['query' => $params]);
        $accessToken = json_decode($accessTokenResponse->getBody(), true);

        // Step 2. Retrieve profile information about the current user.
        $fields = 'id,email,first_name,last_name,link,name,picture,locale';
        
        $profileResponse = $client->request('GET', $urlMe, [
            'query' => [
                'access_token' => $accessToken['access_token'],
                'fields' => $fields
            ]
        ]);

        $profile = json_decode($profileResponse->getBody(), true);

        $profile['accessToken'] = $accessToken['access_token'];
        // Check is user already exists
        
        $profile['isKnowUser'] = false;

        $em= $this->getDoctrine()->getManager();
        $possibleUser = $em->getRepository('MDMondialatorBundle:User')->findBy(array('email' => $profile['email']));

        if(count($possibleUser) > 0){
        	$profile['isKnowUser'] = true;
        	$profile['knowUser'] = $possibleUser[0];

        	return $profile;
        }
        
        
        // Search the country from locale info
        $countryCode = explode("_", $profile['locale'])[1];
        
        $em= $this->getDoctrine()->getManager();
        $country = $em->getRepository('MDMondialatorBundle:Country')
        	->findBy(array('iso' => $countryCode))
        ;
        
        $profile['locale'] = $country[0];

        $profile['picture'] = 'https://graph.facebook.com/v2.5/'.$profile['id'].'/picture?type=large';

		return $profile;
	}

	/**
	 * Handle mail/mdp login (SPEED)
	 * return: users infos
	 */

	public function loginMondialator($params){
		$em = $this->getDoctrine()->getManager();
        $possibleUser = $em->getRepository('MDMondialatorBundle:User')->findBy(array('email' => $params['email']));

        if(count($possibleUser) > 0){
        	$profile['isKnowUser'] = true;
        	if($params["password"] ==  $possibleUser[0]->getPassword()){
        		$profile['knowUser'] = $possibleUser[0];
        		return $profile;
        	}else{
        		return 0;
        	}	
        }
        
	}

	/**
	 * Return user data from user id stocked in json web token
	 * return: users infos
	 */ 

	public function getUserAction(Request $request){
		if($request->getMethod() == 'GET'){

			$jwt = $request->headers->get('authorization');
			$decoded = $this->resolveToken($jwt);

			$repository = $this
				->getDoctrine()
				->getManager()
				->getRepository('MDMondialatorBundle:User')
			;

			$u = $repository->find($decoded->sub);
			
			$follow = $u->getFollowersId();
			$following = $u->getFollowingId();

			$user = $repository->findUserById($decoded->sub)->getArrayResult();

			$user[0]['follow'] = $follow;
			$user[0]['following'] = $following;

			return new JsonResponse(
				array(
					'user' => $user,
					'status' => Response::HTTP_OK
				)
			);
		}

		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));	
	}

 	public function editAction($id){

 		//Récupération de l'user à éditer
 		$repository = $this
 			->getDoctrine()
 			->getManager()
 			->getRepository('MDMondialatorBundle:User')
 		;

 		$user = $repository->find($id);

 		$em= $this->getDoctrine()->getManager();

 		$em->flush();
 		return new Response("OK");
 	}

 	/**
 	 * Create a new follower
 	 */

 	public function addfollowAction(Request $request){
 		if($request->getMethod() == "POST"){
 			
	 		$jwt = $request->headers->get('authorization');
	 		$decoded = $this->resolveToken($jwt);

	 		$userid = $decoded->sub;
	 		$followid = $request->request->get('followid');

	 		if($userid==$followid){
	 			return new JsonResponse(array("status" => Response::HTTP_BAD_REQUEST));
	 		}

	 		$repository = $this
	 			->getDoctrine()
	 			->getManager()
	 			->getRepository('MDMondialatorBundle:User')
	 		;

	 		$user = $repository->find($userid);
	 		$following = $repository->find($followid);

			//Regarder si ce follow débloque un achievement
			$this->checkAchievementFollowAction($following);	 		

	 		$user->setFollowing($following);

	 		$em = $this->getDoctrine()->getManager();
	 		$em->persist($user);
	 		$em->flush();

	 		return new JsonResponse(array("status" => Response::HTTP_OK));
 		}

 		return new JsonResponse(array("status" => Response::HTTP_BAD_REQUEST));		
 	}

 	/**
 	 * Delete a follower
 	 */

 	public function removefollowAction(Request $request){
 		if($request->getMethod() == "POST"){

	 		$jwt = $request->headers->get('authorization');
	 		$decoded = $this->resolveToken($jwt);
	 		
	 		$userid = $decoded->sub;
	 		$followid = $request->request->get('followid');

	 		if($userid==$followid){
	 			return new JsonResponse(array("status" => Response::HTTP_BAD_REQUEST));
	 		}
	 		
	 		$repository = $this
	 			->getDoctrine()
	 			->getManager()
	 			->getRepository('MDMondialatorBundle:User')
	 		;
	 		
	 		$user = $repository->find($userid);
	 		$following = $repository->find($followid);
	 		
	 		$user->removeFollowing($following);
	 		
	 		$em = $this->getDoctrine()->getManager();
	 		$em->persist($user);
	 		$em->flush();
	 		
	 		return new JsonResponse(array("status" => Response::HTTP_OK));
	 	}
	 	return new JsonResponse(array("status" => Response::HTTP_BAD_REQUEST));	
 	}

 	/**
 	 * Return user's followers based on ids
 	 */

 	public function getFollowerData(Request $request){

 	}

 	/**
 	 * Return users that are following an user based on ids
 	 */

 	public function getFollowingData(Request $request){

 	}

 	/**
 	 * Return the user score
 	 */
 	public function getScoreAction(Request $request){
 		if($request->getMethod() == "GET"){
 			$jwt = $request->headers->get('authorization');
 			$decoded = $this->resolveToken($jwt);
 			
 			$userid = $decoded->sub;

 			$em = $this->getDoctrine()->getManager();
 			$repository = $em->getRepository('MDMondialatorBundle:User');

 			$u = $repository->find($userid);

 			return new JsonResponse(array(
 					'status' => Response::HTTP_OK,
 					'score' => $u->getScore()
 				)
 			);
 		}

 		return new JsonResponse(array("status" => Response::HTTP_BAD_REQUEST));
 	}

 	/**
 	 * Return the user followers
 	 */
 	public function getFollowersAction(Request $request){
 		if($request->getMethod() == "GET"){
 			$jwt = $request->headers->get('authorization');
 			$decoded = $this->resolveToken($jwt);
 			
 			$userid = $decoded->sub;

 			$em = $this->getDoctrine()->getManager();
 			$repository = $em->getRepository('MDMondialatorBundle:User');

 			$u = $repository->find($userid);

 			return new JsonResponse(array(
 					'status' => Response::HTTP_OK,
 					'follows' => $u->getFollowersId()
 				)
 			);
 		}

 		return new JsonResponse(array("status" => Response::HTTP_BAD_REQUEST));
 	}

 	/**
 	 * Return the user achievements
 	 */
 	public function getAchievementsAction(Request $request){
 		if($request->getMethod() == "GET"){
 			$jwt = $request->headers->get('authorization');
 			$decoded = $this->resolveToken($jwt);
 			
 			$userid = $decoded->sub;

 			$em = $this->getDoctrine()->getManager();
 			$repository = $em->getRepository('MDMondialatorBundle:User');

 			$u = $repository->find($userid);

 			$achievements = $u->getAchievement();

 			$repository = $em->getRepository('MDMondialatorBundle:Achievement');

 			$achievement =  $repository->findbyId($achievements[count($achievements) - 1]->getId())->getArrayResult();

 			return new JsonResponse(array(
 					'status' => Response::HTTP_OK,
 					'achievements' => $achievement
 				)
 			);
 		}

 		return new JsonResponse(array("status" => Response::HTTP_BAD_REQUEST));
 	}


 	/**
     * Generate JSON Web Token.
     */
    protected function createToken($user){
        $payload = [
            'sub' => $user->getId(),
            'iat' => time(),
            'exp' => time() + (2 * 7 * 24 * 60 * 60)
        ];
        return JWT::encode($payload, $this->getParameter('secret'));
    }

    /**
	 * 
	 */ 
    protected function resolveToken($jwt){

    	if (strpos($jwt, 'Bearer') !== false) {
		    $jwt = explode("Bearer ", $jwt)[1];
		}
		
		$decoded = JWT::decode($jwt, $this->getParameter('secret'),array('HS256'));

		return $decoded;
    }  

    public function checkAchievementFollowAction($user){

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:AchievementType')
        ;     

    	$achievementsTypeFollow = $repository->findOneByName('Follow');

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Achievement')
        ;     

    	$achievementsFollow = $repository->findByType($achievementsTypeFollow);

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:User')
        ;     	

    	$followingBdd = $repository->findOneById($user->getId());

    	$userNbFollow =(int) count((array)$followingBdd->getFollowers());

    	$userAchievements = $user->getAchievement();

    	$userAchArray[]= array();
    	foreach ($userAchievements as $achievement) {
    		$userAchArray[] = $achievement;
    	}

    	foreach ($achievementsFollow as $achievement) {
    		if((int)$achievement->getRank()<=$userNbFollow && !(in_array($achievement, $userAchArray))){
				$user->setAchievement($achievement);
    		}
    	}

		$em = $this->getDoctrine()->getManager();
 		$em->persist($user);
 		$em->flush();   

		return new JsonResponse(
			array(
				'status' => Response::HTTP_OK, 
			)
		); 

	}    
}
