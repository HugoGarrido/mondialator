<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use MD\MondialatorBundle\Entity\SocialType;

class SocialTypeController extends Controller
{
    public function getSocialTypeAction()
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:SocialType')
        ;

        $listSocialType = $repository->findAll();

        return $this->render('MDMondialatorBundle:SocialType:getSocialType.html.twig', array(
                'listSocialType'=>$listSocialType
            )
        );
    }

    public function addSocialTypeAction(Request $request)
    {
        $newSocialType = new SocialType();

        $formBuilder = $this->get('form.factory')->createBuilder('form', $newSocialType);

        $formBuilder
            ->add('name', 'text')
            ->add('save','submit')
        ;

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newSocialType);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirect($this->generateUrl('mondialator_social_types') );
        }


        return $this->render('MDMondialatorBundle:SocialType:addSocialType.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    public function editSocialTypeAction(Request $request, $id)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:SocialType')
        ;

        $newSocialType= $repository->findOneById($id);

        $formBuilder = $this->get('form.factory')->createBuilder('form', $newSocialType);

        $formBuilder
            ->add('name', 'text')
            ->add('save','submit')
        ;

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newSocialType);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirect($this->generateUrl('mondialator_social_types') );
        }

        return $this->render('MDMondialatorBundle:SocialType:editSocialType.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    public function removeSocialTypeAction($id){
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:SocialType')
        ;

        $socialType = $repository->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($socialType);
        $em->flush();

        return $this->render('MDMondialatorBundle:SocialType:removeSocialType.html.twig');

    }
}
