<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use \Firebase\JWT\JWT;

class LeaderboardController extends Controller
{
	public function getAction(Request $request, $offset){

		if($request->getMethod() == 'GET'){

			if(!isset($offset)){
				$offset = 0;
			}

			$jwt = $request->headers->get('authorization');
			$decoded = $this->resolveToken($jwt);

			$em = $this->getDoctrine()->getManager();
			$repository = $em->getRepository('MDMondialatorBundle:User');

			$leaderboard = $repository->getGlobalLeaderboard($offset)->getArrayResult();

			return new JsonResponse(
				array(
					'leaderboard' => $leaderboard,
					'status' => Response::HTTP_OK
				)
			);
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));	
	}

    /**
	 * 
	 */ 
    protected function resolveToken($jwt){

    	if (strpos($jwt, 'Bearer') !== false) {
		    $jwt = explode("Bearer ", $jwt)[1];
		}
		
		$decoded = JWT::decode($jwt, $this->getParameter('secret'),array('HS256'));

		return $decoded;
    }  
}
