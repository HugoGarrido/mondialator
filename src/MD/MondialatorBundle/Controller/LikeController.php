<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp;
use \Firebase\JWT\JWT;

use MD\MondialatorBundle\Entity\Thumb;


class LikeController extends Controller
{
    public function likeAddAction(Request $request)
    {
		if($request->getMethod() == 'POST'){

			$jwt = $request->headers->get('authorization');
			$decoded = $this->resolveToken($jwt);

			$repository = $this
				->getDoctrine()
				->getManager()
				->getRepository('MDMondialatorBundle:User')
			;

			$u = $repository->find($decoded->sub);
			
			$user = $repository->findUserById($decoded->sub)->getArrayResult();   	

			$userId=$user[0]['id'];

	 		$submissionId =(int) $request->request->get('submissionId');

	        $repository = $this
	            ->getDoctrine()
	            ->getManager()
	            ->getRepository('MDMondialatorBundle:User')
	        ;


        	$userBdd = $repository->findOneById($userId);

	        $repository = $this
	            ->getDoctrine()
	            ->getManager()
	            ->getRepository('MDMondialatorBundle:Submission')
	        ;     

        	$submissionBdd = $repository->findOneById($submissionId);

        	$like = new Thumb();

        	$like->setUser($userBdd);
        	$like->setSubmission($submissionBdd);

		    $em = $this->getDoctrine()->getManager();
		    $em->persist($like);
		    $em->flush();

			//On regarde si le like débloque un achievement
        	$submissionBdd->getUser()->setNbLike($submissionBdd->getUser()->getNbLike()+1);			
			$this->checkAchievementLikeAction($submissionBdd->getUser());
			//On donne les points gagnés grace au like
			$this->givePoint($submissionBdd);					    

			return new JsonResponse(
				array(
					'userId'=>$userId,
					'scoreTest'=>$submissionBdd->getUser()->getScore()+2,
					'status' => Response::HTTP_OK, 
				)
			);   
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));	
	}

	public function likeRemoveAction(Request $request){
		if($request->getMethod() == 'POST'){

			$jwt = $request->headers->get('authorization');
			$decoded = $this->resolveToken($jwt);

			$repository = $this
				->getDoctrine()
				->getManager()
				->getRepository('MDMondialatorBundle:User')
			;

			$u = $repository->find($decoded->sub);
			
			$user = $repository->findUserById($decoded->sub)->getArrayResult();   	

			$userId=$user[0]['id'];

	 		$submissionId =(int) $request->request->get('submissionId');

	        $repository = $this
	            ->getDoctrine()
	            ->getManager()
	            ->getRepository('MDMondialatorBundle:User')
	        ;


        	$userBdd = $repository->findOneById($userId);

	        $repository = $this
	            ->getDoctrine()
	            ->getManager()
	            ->getRepository('MDMondialatorBundle:Submission')
	        ;     

        	$submissionBdd = $repository->findOneById($submissionId);

	        $repository = $this
	            ->getDoctrine()
	            ->getManager()
	            ->getRepository('MDMondialatorBundle:Thumb')
	        ; 

        	$likesUser = $repository->findByUser($userBdd);

        	$em = $this->getDoctrine()->getManager();
        	foreach ($likesUser as $like) {
        		if($like->getSubmission()->getId()==$submissionId){
				    $em->remove($like);
				    $em->flush();        			
        		}
        	}

			//On enlève les points gagnés grace au like
			$this->removePoint($submissionBdd);					    

			return new JsonResponse(
				array(
					'userId'=>$userId,
					'status' => Response::HTTP_OK, 
				)
			);   
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));	
	}

    protected function resolveToken($jwt){

    	if (strpos($jwt, 'Bearer') !== false) {
		    $jwt = explode("Bearer ", $jwt)[1];
		}
		
		$decoded = JWT::decode($jwt, $this->getParameter('secret'),array('HS256'));

		return $decoded;
    }

   public function checkAchievementLikeAction($user){

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:AchievementType')
        ;     

    	$achievementsTypeLike = $repository->findOneByName('Like');

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Achievement')
        ;     

    	$achievementsLike = $repository->findByType($achievementsTypeLike);


    	$userNbLike =(int) $user->getNbLike();

    	$userAchievements = $user->getAchievement();

    	$userAchArray[]= array();
    	foreach ($userAchievements as $achievement) {
    		$userAchArray[] = $achievement;
    	}

    	foreach ($achievementsLike as $achievement) {
    		if((int)$achievement->getRank()<=$userNbLike && !(in_array($achievement, $userAchArray))){
				$user->setAchievement($achievement);
    		}
    	}

		$em = $this->getDoctrine()->getManager();
 		$em->persist($user);
 		$em->flush();   

		return new JsonResponse(
			array(
				'status' => Response::HTTP_OK, 
			)
		); 

	}

	public function givePoint($submission){

		$submission->getUser()->setScore($submission->getUser()->getScore()+2);

	    $em = $this->getDoctrine()->getManager();
	    $em->persist($submission->getUser());
	    $em->flush();

		return new JsonResponse(
			array(
				'status' => Response::HTTP_OK, 
			)
		);		
	}

	public function removePoint($submission){

		$submission->getUser()->setScore($submission->getUser()->getScore()-2);

	    $em = $this->getDoctrine()->getManager();
	    $em->persist($submission->getUser());
	    $em->flush();

		return new JsonResponse(
			array(
				'status' => Response::HTTP_OK, 
			)
		);		
	}	
}
