<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use MD\MondialatorBundle\Entity\Country;

class CountryController extends Controller
{

    public function getCountriesAction()
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Country')
        ;

        $listCountry = $repository->findAll();

        return $this->render('MDMondialatorBundle:Country:getCountry.html.twig', array(
                'listCountry'=>$listCountry
            )
        );
    }

    public function addCountryAction(){
        return $this->render('MDMondialatorBundle:Country:addCountry.html.twig', array()
        );
    }

    public function editCountryAction(){
        return $this->render('MDMondialatorBundle:Country:editCountry.html.twig', array()
        );
    }

    public function removeCountryAction(){
        return $this->render('MDMondialatorBundle:Country:removeCountry.html.twig', array()
        );
    }

}
