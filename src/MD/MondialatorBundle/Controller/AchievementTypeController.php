<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use MD\MondialatorBundle\Entity\AchievementType;


class AchievementTypeController extends Controller
{
    public function getAchievementTypesAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:AchievementType')
        ;

        $listAchievementType = $repository->findAll();

        return $this->render('MDMondialatorBundle:AchievementType:getAchievementTypes.html.twig', array(
                'listAchievementType'=>$listAchievementType
            )
        );
    }

    public function addAchievementTypeAction(Request $request)
    {

        $newAchievementType = new AchievementType();


        $formBuilder = $this->get('form.factory')->createBuilder('form', $newAchievementType);

        $formBuilder
            ->add('name', 'text')
            ->add('save','submit')
        ;

        $form = $formBuilder->getForm();

        //Envoie du form :
        $form->handleRequest($request);


        if ($form->isValid()) {
            // On l'enregistre notre objet $advert dans la base de données, par exemple
            $em = $this->getDoctrine()->getManager();
            $em->persist($newAchievementType);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirect($this->generateUrl('mondialator_achievement_types'));
        }


        return $this->render('MDMondialatorBundle:AchievementType:addAchievementType.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    public function editAchievementTypeAction(Request $request,$id)
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:AchievementType')
        ;

        $newAchievementType = $repository->findOneById($id);

        $formBuilder = $this->get('form.factory')->createBuilder('form', $newAchievementType);

        $formBuilder
            ->add('name', 'text')
            ->add('save','submit')
        ;

        $form = $formBuilder->getForm();

        //Envoie du form :
        $form->handleRequest($request);


        if ($form->isValid()) {
            // On l'enregistre notre objet $advert dans la base de données, par exemple
            $em = $this->getDoctrine()->getManager();
            $em->persist($newAchievementType);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirect($this->generateUrl('mondialator_achievement_types'));
        }


        return $this->render('MDMondialatorBundle:AchievementType:editAchievementType.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    public function removeAchievementTypeAction($id){
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:AchievementType')
        ;

        $achievementType = $repository->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($achievementType);
        $em->flush();

        return $this->render('MDMondialatorBundle:AchievementType:removeAchievementType.html.twig');

    }

}
