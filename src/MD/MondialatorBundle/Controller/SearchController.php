<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    public function basicAction(Request $request, $slug)
    {
        if($request->getMethod() == "GET"){

    		$repository = $this
    			->getDoctrine()
    			->getManager()
    			->getRepository('MDMondialatorBundle:User')
    		;

    		/*
    		 * Search for user's friend
    		 */

    		/*
    		 * Search for users
    		 */
    		$usersFirst = $repository->findUsersByFirstName($slug)->getArrayResult();
    		$usersLast = $repository->findUsersByLastName($slug)->getArrayResult();
    		
    		$users = array_merge($usersFirst, $usersLast);

    		/*
    		 * Search for Day
    		 */
    		
    		/*
    		 * Search for Sponsors
    		 */

    	    return new JsonResponse([
    	    	"data" => [
    	    		"users" => $users
    	    	],
    	    	"status" => Response::HTTP_OK
    	    	]
    	    );
        }
        
        return new JsonResponse(["status" => Response::HTTP_BAD_REQUEST]);
    }
}
