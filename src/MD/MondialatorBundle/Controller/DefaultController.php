<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MDMondialatorBundle:Default:index.html.twig');
    }
}
