<?php

namespace MD\MondialatorBundle\Controller;

use Firebase\JWT\JWT;

use MD\MondialatorBundle\Entity\Submission;
use MD\MondialatorBundle\Entity\Media;
use MD\MondialatorBundle\Form\SubmissionType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SubmissionController extends Controller
{
	public function addAction(Request $request){
		
		if($request->getMethod() =='POST'){
			
			//Since dropzone does not use angular http module, the token is sended
			//using formData

			$jwt = $request->request->get('authorization');
			$decoded = $this->resolveToken($jwt);
			$userid = $decoded->sub;

			$em= $this->getDoctrine()->getManager();
			
			$media = new Media();

			// 1 - creating the file object
			$media->setFile($request->files->get('file'));
			$extMedia = $media->getExtension();
			$repository = $em->getRepository('MDMondialatorBundle:MediaType');

			
			// 2 - Check challenge type
			$repository = $em->getRepository('MDMondialatorBundle:Challenge');
			$challenge = $repository->find($request->request->get('challengeid'));	
			$challengeType = $challenge->getType()->getName();
			
			
			if($challenge->getType() == NULL){
				//Par défaut : le type requis est une image
				$challengeType = "image";
			}				
			
			// Return an error if the file uploaded is not correct
			if($challenge->getType()->checkType($extMedia) != true){
				return new JsonResponse(array(
						'status' => Response::HTTP_NOT_ACCEPTABLE,
						'error' => 'your media does not match'
					)
				);
			}
			
			// 2 - fetching the user with its id
			$repository = $em->getRepository('MDMondialatorBundle:User');
			$user = $repository->find($userid);

			// 4 - create the submission entry if no entries yet
			$repository = $em->getRepository('MDMondialatorBundle:Submission');
			$exist = $repository->exist($user, $challenge);

			
			if(!$exist){

				$sub = new Submission();

				$media->setType($challenge->getType());
				$em->persist($media);

				$sub->setUser($user);
				$sub->setChallenge($challenge);
				$sub->setMedia($media);
				$sub->setPosted(true);

				$em->persist($sub);
				$em->flush();

				//On vérifie si la nouvelle submission fait gagner un achievement
				$user->setNbSubmission($user->getNbSubmission()+1);
				$this->checkAchievementSubmissionAction($user);

				//On attribu les points gagnés pour la submission
				$user->setScore($user->getScore()+10);
			    $em = $this->getDoctrine()->getManager();
			    $em->persist($user);
			    $em->flush();				

				return new JsonResponse( 
					array(
						"status" => Response::HTTP_OK
					)
				);
			}
			else{
				return new JsonResponse(
					array(
						"status" => Response::HTTP_NOT_ACCEPTABLE
					)
				);
			}
			
		}	
		return new JsonResponse(
			array(
				"status" => Response::HTTP_BAD_REQUEST
			)
		);
	}

    public function checkAchievementSubmissionAction($user){

	        $repository = $this
	            ->getDoctrine()
	            ->getManager()
	            ->getRepository('MDMondialatorBundle:AchievementType')
	        ;     

        	$achievementsTypeSubmission = $repository->findOneByName('Submission');

	        $repository = $this
	            ->getDoctrine()
	            ->getManager()
	            ->getRepository('MDMondialatorBundle:Achievement')
	        ;     

        	$achievementsSubmission = $repository->findByType($achievementsTypeSubmission);


        	$userNbSubmission =(int) $user->getNbSubmission();

        	$userAchievements = $user->getAchievement();

        	$userAchArray[]= array();
        	foreach ($userAchievements as $achievement) {
        		$userAchArray[] = $achievement;
        	}

        	foreach ($achievementsSubmission as $achievement) {
        		if((int)$achievement->getRank()<=$userNbSubmission && !(in_array($achievement, $userAchArray))){
    				$user->setAchievement($achievement);
        		}
        	}

			$em = $this->getDoctrine()->getManager();
	 		$em->persist($user);
	 		$em->flush();   

		return new JsonResponse(
			array(
				'status' => Response::HTTP_OK, 
			)
		); 
	}


    /**
	 * 
	 */ 
    protected function resolveToken($jwt){

    	if (strpos($jwt, 'Bearer') !== false) {
		    $jwt = explode("Bearer ", $jwt)[1];
		}
		
		$decoded = JWT::decode($jwt, $this->getParameter('secret'),array('HS256'));

		return $decoded;
    }	

    public function checkAction(Request $request, $challengeId){
    	
    	if($request->getMethod() == 'GET'){
			$jwt = $request->headers->get('authorization');
			$decoded = $this->resolveToken($jwt);
			$userid = $decoded->sub;

			$em= $this->getDoctrine()->getManager();

			$repository = $em->getRepository('MDMondialatorBundle:User');
			$user = $repository->find($userid);		

			$repository = $em->getRepository('MDMondialatorBundle:Challenge');
			$challenge = $repository->find($challengeId);		

			$repository = $em->getRepository('MDMondialatorBundle:Submission');

	    	return new JsonResponse(
				array(
					'status' => Response::HTTP_OK,
					'existingSubmission' => $repository->exist($user, $challenge)
				)
			);
		}
		else{
	    	return new Response(Response::HTTP_BAD_REQUEST);
		}
    }
}
