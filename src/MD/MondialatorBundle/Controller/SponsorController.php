<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use MD\MondialatorBundle\Form\MediaForm;
use MD\MondialatorBundle\Entity\Sponsor;

class SponsorController extends Controller
{
    public function getSponsorAction()
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Sponsor')
        ;

        $listSponsor = $repository->findAll();

        return $this->render('MDMondialatorBundle:Sponsor:getSponsor.html.twig', array(
                'listSponsor'=>$listSponsor
            )
        );
    }

    public function addSponsorAction(Request $request)
    {
        $newSponsor = new Sponsor();

        $formBuilder = $this->get('form.factory')->createBuilder('form', $newSponsor);

        $formBuilder
            ->add('name', 'text')
            ->add('description', 'textarea')
            ->add('url', 'text')
            ->add('picture', new MediaForm())
            ->add('save','submit')
        ;

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($newSponsor);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirect($this->generateUrl('mondialator_sponsors'));
        }

        return $this->render('MDMondialatorBundle:Sponsor:addSponsor.html.twig', array(
                'form' => $form->createView()
            )
        );
    }


    public function editSponsorAction(Request $request,$id)
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Sponsor')
        ;

        $newSponsor = $repository->findOneById($id);

        $formBuilder = $this->get('form.factory')->createBuilder('form', $newSponsor);

        $formBuilder
            ->add('name', 'text')
            ->add('description', 'textarea')
            ->add('url', 'text')
            ->add('picture', new MediaForm())
            ->add('save','submit')
        ;

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($newSponsor);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirect($this->generateUrl('mondialator_sponsors'));
        }


        return $this->render('MDMondialatorBundle:Sponsor:editSponsor.html.twig', array(
                'form' => $form->createView()
            )
        );
    }


    public function removeSponsorAction($id){
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('MDMondialatorBundle:Sponsor')
        ;

        $sponsor = $repository->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($sponsor);
        $em->flush();

        return $this->render('MDMondialatorBundle:Sponsor:removeSponsor.html.twig');

    }
}
