<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use \Firebase\JWT\JWT;

class FeedController extends Controller
{
	public function getAction(Request $request, $client, $offset){

		if($request->getMethod() == 'GET'){

			if(!isset($offset)){
				$offset = 0;
			}

			$jwt = $request->headers->get('authorization');
			$decoded = $this->resolveToken($jwt);

			$em = $this->getDoctrine()->getManager();
			$repository = $em->getRepository('MDMondialatorBundle:User');

			$user = $repository->find($decoded->sub);

			
			$repository = $em->getRepository('MDMondialatorBundle:Submission');
			
			if($client == "user"){
				$subs = $repository->getSubmissionsFeed($user, $offset)->getArrayResult();	
			}
			
			if($client == "world"){
				$subs = $repository->getGlobalSubmissionsFeed($offset)->getArrayResult();	
			}

			$repositoryT = $em->getRepository('MDMondialatorBundle:Thumb');
			$repositoryC = $em->getRepository('MDMondialatorBundle:Comment');
			
			for($i=0; $i<count($subs); $i++){
				$subs[$i]['thumbs'] = $repositoryT->getThumbsBySubmissionId($subs[$i]['id'])->getArrayResult();
				$subs[$i]['thumbsLength'] = count($subs[$i]['thumbs']);

				$userLiked = false;

				foreach($subs[$i]['thumbs'] as $thumb){
					if($thumb['user']['id'] == $user->getId()){
						$userLiked = true;
						break;
					}
				}

				$subs[$i]['userLiked'] = $userLiked;

				$subs[$i]['comments'] = $repositoryC->getCommentsBySubmissionId($subs[$i]['id'])->getArrayResult();
				$subs[$i]['commentsLength'] = count($subs[$i]['comments']);
			}

			return new JsonResponse(
				array(
					'submissions' => $subs,
					'status' => Response::HTTP_OK
				)
			);
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));	
	}

    /**
	 * 
	 */ 
    protected function resolveToken($jwt){

    	if (strpos($jwt, 'Bearer') !== false) {
		    $jwt = explode("Bearer ", $jwt)[1];
		}
		
		$decoded = JWT::decode($jwt, $this->getParameter('secret'),array('HS256'));

		return $decoded;
    }  
}
