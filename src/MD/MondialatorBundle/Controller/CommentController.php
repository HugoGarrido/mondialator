<?php

namespace MD\MondialatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp;
use \Firebase\JWT\JWT;

use MD\MondialatorBundle\Entity\Comment;


class CommentController extends Controller
{
    public function commentAddAction(Request $request)
    {
		if($request->getMethod() == 'POST'){

			$jwt = $request->headers->get('authorization');
			$decoded = $this->resolveToken($jwt);

			$repository = $this
				->getDoctrine()
				->getManager()
				->getRepository('MDMondialatorBundle:User')
			;

			$u = $repository->find($decoded->sub);
			
			$user = $repository->findUserById($decoded->sub)->getArrayResult();   	

			$userId=$user[0]['id'];

	 		$submissionId =(int) $request->request->get('submissionId');

	        $repository = $this
	            ->getDoctrine()
	            ->getManager()
	            ->getRepository('MDMondialatorBundle:User')
	        ;


        	$userBdd = $repository->findOneById($userId);

	        $repository = $this
	            ->getDoctrine()
	            ->getManager()
	            ->getRepository('MDMondialatorBundle:Submission')
	        ;     

        	$submissionBdd = $repository->findOneById($submissionId);

        	$comment = new Comment();

        	$comment->setUser($userBdd);
        	$comment->setSubmission($submissionBdd);
        	$comment->setText($request->request->get('textComment'));

		    $em = $this->getDoctrine()->getManager();
		    $em->persist($comment);
		    $em->flush();

			return new JsonResponse(
				array(
					'userId'=>$userId,
					'submissionId'=>$submissionId,
					'status' => Response::HTTP_OK, 
				)
			);   
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));	
	}

	public function commentbySubmissionAction(Request $request, $id){
		if($request->getMethod() == 'GET'){
			
			$em = $this->getDoctrine()->getManager();	
			$repository = $em->getRepository('MDMondialatorBundle:Comment');
			
			$comments = $repository->getCommentsBySubmissionId($id)->getArrayResult();

			return new JsonResponse(
				array(
					'comments' => $comments,
					'status' => Response::HTTP_OK
				)
			);
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));	
	}

	public function commentDeleteAction(Request $request){
		if($request->getMethod() == 'POST'){

			$jwt = $request->headers->get('authorization');
			$decoded = $this->resolveToken($jwt);

			$userId = (int) $request->request->get('userId');

			if($decoded->sub == $userId){
		 		$commentId =(int) $request->request->get('commentId');

			    $em = $this->getDoctrine()->getManager();
			    $repository = $em->getRepository('MDMondialatorBundle:Comment');
			    $comment = $repository->find($commentId);
			    $em->remove($comment);
			    $em->flush();

				return new JsonResponse(
					array(
						'status' => Response::HTTP_OK, 
					)
				);
			}
			else{
				return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));
			}	 
		}
		return new JsonResponse(array('status' => Response::HTTP_BAD_REQUEST));
	}

    protected function resolveToken($jwt){

    	if (strpos($jwt, 'Bearer') !== false) {
		    $jwt = explode("Bearer ", $jwt)[1];
		}
		
		$decoded = JWT::decode($jwt, $this->getParameter('secret'),array('HS256'));

		return $decoded;
    } 		

}
